import React, { Component } from 'react';
import List from './List';
import store from './store';
import { Provider } from 'react-redux';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      name: ' ',
      content: ' '
    }
    this.handlechange=this.handlechange.bind(this);
    this.handlesubmit=this.handlesubmit.bind(this);
  };
  handlechange(e){
      this.setState({[e.target.name]: e.target.value})
      this.setState({[e.target.content]: e.target.value})
      //console.log(this.state);
  }
  handlesubmit(e){
    //e.preventDefault();
    e.preventDefault();
    console.log(this.state.name);
    console.log(this.state.content);
    
  }


  render() {
    //console.log(this.props.posts);
    
    return (
      <Provider store={store}>
      <div className="App">
        <form onSubmit={this.handlesubmit}>
          <label>
                Name:
                  <input type="text" name="name" onChange={this.handlechange}/>
                Content:
                  <textarea name="content" onChange={this.handlechange}/>
          </label>  
                 
                  < button type="submit">Submit</button>
        </form>
        
         <List/>
      </div>
      </Provider>

    );

  }
}

export default App;
