import React , {Component}  from 'react';
import {fetchPosts} from './actions';

import { connect } from 'react-redux';

class List extends Component{
    constructor() {
        super();
        this.state = {
            posts: []
        };
    }

    componentDidMount(){
        //called automatically when the component gets attached to the dom
        this.props.fetchPosts();
    }
    render(){
        console.log(this.props.posts);
        if(this.props.posts && props.posts.posts && this.props.posts.posts.length>0){
        return(
            <ul>
                {
                    this.state.posts.map(function(post) {
                        return (
                            <li>
                                <p>Title - { post.title }</p>
                                <p>Content - { post.body }</p>
                            </li>
                        )
                    })
                }
            </ul>
        )
    }
    else{
        <h2> hello </h2>
    }
  }
}
const mapStateToProps = (state)=>{
    return {
      posts: state.posts
    };
  };
  
  const mapDispatchToProps = (dispatch)=>{
    return {
      fetchPosts: () => dispatch(fetchPosts())
    };
  };

export default connect(mapStateToProps,mapDispatchToProps)(List);
