export function fetchPosts(){
    return function(dispatch){
        fetch('https://localhost:9000')
         .then(res=>res.json())
         .then(posts=>dispatch({
             type: 'FETCH_POSTS',
             data: posts
         }));
    }
}


export function addPost(post){
    return{
        type: 'ADD_POST',
        data:{
            title: post.title,
            content: post.content
        }
    };
}