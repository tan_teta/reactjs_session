import React, { Component } from 'react';
import { connect } from 'react-redux';

import { addPost } from '../actions';

class BlogForm extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      content: ''
    };

    this.onChange = this.onChange.bind(this);
    this.formSubmit = this.formSubmit.bind(this);
  }

  onChange(e) {
    this.setState({[e.target.name]: e.target.value});
  }

  formSubmit(e) {
    debugger;
    e.preventDefault();
    this.props.addPost(this.state);
    this.setState({
      title: '',
      content: ''
    });
  }

  render() {
    return (
      <form onSubmit={ this.formSubmit }>
        Name: <input type="text" name="title" value={ this.state.title } onChange={ this.onChange }/><br/>
        Content: <input type="textarea" name="content" value={ this.state.content } onChange={ this.onChange }/><br/>
        <button type="submit">Submit</button>
      </form>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    addPost: (post) => dispatch(addPost(post))
  };
}

export default connect(null, mapDispatchToProps)(BlogForm);