import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions';

class List extends Component {
  constructor() {
    super();
    // this.state = {
    //   posts: [
    //     {title: 'a', content: 'content-a'},
    //     {title: 'b', content: 'content-b'},
    //     {title: 'c', content: 'content-c'},
    //     {title: 'd', content: 'content-d'}
    //   ]
    // };
  }

  componentDidMount() {
    this.props.fetchPosts();
  }

  render() {
    console.log(this.props.posts);

    // const { posts } = this.props;
    if(this.props.posts && this.props.posts.posts && this.props.posts.posts.length > 0) {
      return (
        <ul>
          {
            posts.posts.map(function(post) {
              return (
                <li>
                  <p>Title - { post.title }</p>
                  <p>Content - { post.body }</p>
                </li>
              )
            })
          }
        </ul>
      );
    } else {
      return (
        <h3>Loading...</h3>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    posts: state.posts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchPosts: () => dispatch(fetchPosts())
  }; 
}

export default connect(mapStateToProps, mapDispatchToProps)(List);