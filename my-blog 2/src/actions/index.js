export function fetchPosts() {
  return function(dispatch) {
    fetch(`https://jsonplaceholder.typicode.com/posts`)
      .then(res => res.json())
      .then(posts => dispatch({
        type: 'FETCH_POSTS',
        data: posts
      }));
  }
}

export function addPost(post) {
  return {
    type: 'ADD_POST',
    data: {
      title: post.title,
      content: post.content
    }
  };
}