const postReducer = (state = null, action) => {
  switch(action.type) {
    case 'ADD_POST':
      debugger;
      return {
        ...state,
        posts: [{'title': action.data.title, 'body': action.data.content}, ...state.posts]
      };

    case 'FETCH_POSTS':
      return {
        ...state,
        posts: action.data
      };
      
    default:
      return state;
  }
}

export default postReducer;