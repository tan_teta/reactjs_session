import React, { Component } from 'react';
import BlogForm from './components/blog-form';
import List from './components/list';


import store from './store';
import { Provider } from 'react-redux';

// const store = createStore(() => [], {}, applyMiddleware());

class App extends Component {
  render() {
    return (
      <Provider store={ store }>
        <div className="App">
          Hello, World!
          <BlogForm/>
          <br/>
          <List/>
        </div>
      </Provider>
    );
  }
}

export default App;
